export default {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
    LOCATIONS: 'locations',
    TICKETS: 'tickets'
}