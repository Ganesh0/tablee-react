import axiosCall from './index';
import types from './action-types';
import config from '../../config/config';
import dictionary from '../../config/static/dictionary';


export const getItems = () => {
    const ticketNumber = 239282;
    const locationId = 'TL494z9c';
    const url = `${config.BASE_URL}${dictionary.LOCATIONS}/${locationId}/${dictionary.TICKETS}/${ticketNumber}/`;
    const responseType = types.BILL;
    return axiosCall('get', url, responseType);
}