import axios from 'axios';

const headers = {
    "Api-Key": "6fccc7e67e2642d2a9637dd6266e5e60",
}


export default function axiosCall(method, url, responseType, data) {
    return async dispatch => {
        const apiData = data ? { method, url, data, headers } : { method, url, headers };
        axios(apiData)
            .then(response => {
                if (response.data) {
                    dispatch({ type: responseType, updatePayload: response.data });
                    return response.data;
                }
            })
            .catch(err => {
                dispatch({
                    type: `${responseType}_REJECTED`,
                    updatePayload: err.response
                });
                return err.response;
            });
    };
}