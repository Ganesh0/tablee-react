import { combineReducers } from 'redux';
import Bill from './bill.reducer';


const rootReducer = combineReducers({
  Bill,
});

export default rootReducer;