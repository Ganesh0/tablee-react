import types from '../actions/action-types';

export default (state = [], action) => {
    switch (action.type) {
        case types.BILL:
        case `${types.BILL}_REJECTED`:
            return {
                ...state,
                billResponse: action.updatePayload
            }
        default:
            return state;
    }

}