import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Bill  from '../src/components/bill/bill';
import Header from '../src/components/header/index';


const history = createBrowserHistory();

class Routing extends Component {
    render() {
        return (         
                <Router history={history}>
                    <Switch>
                        <Route exact path='/' component={Bill} />
                        <Route exact path='/bill' component={Bill} />
                        <Route exact path='/header' component={Header} />
                    </Switch>
                </Router>
        );
    }
}

export default Routing;