import React, { Component } from 'react';
import { Card, Button, Row, Container, ButtonGroup, ToggleButton, ToggleButtonGroup, Col } from 'react-bootstrap';
import bill from './bill.css';
import Header from '../header/index';
import image from '../../assets/images/google.png';
import config from '../../config/config';
import dictionary from '../../config/static/dictionary';
import WithAction from './index';



class Bill extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeButton: "default",
            items: [],
            tips: 0,
            total: 0
        };
    }
    componentDidMount() {
        const { getItems } = this.props;
        getItems();

        // directly call the api using fetch

        // const ticketNumber = 239282;
        // const locationId = 'TL494z9c';
        // const url = `${config.BASE_URL}${dictionary.LOCATIONS}/${locationId}/${dictionary.TICKETS}/${ticketNumber}/`;
        // fetch(url, {
        //     "method": dictionary.GET,
        //     "headers": {
        //       "Api-Key": "6fccc7e67e2642d2a9637dd6266e5e60",
        //     }
        //   })
        //   .then(response => response.json())
        //   .then(response => {
        //     //   console.log('-------------------response-----------', response)
        //     this.setState({
        //       items: response
        //     });
        //   })
        //   .catch(err => { console.log(err); 
        //   });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps && nextProps.bill) {
            this.setState({ items: nextProps.bill, total: nextProps.bill.totals.total });
        }
    }
    // show the button active when click and add the tips percentage user clicks to the total amount 
    isActive = (e) => {
        let { total } = this.state;
        if (e) {
            let addTips = null;
            switch (e.value) {
                case "active":
                    this.setState({ tips: 0 });
                    break;
                case "18":
                    addTips = (18 / 100) * total;
                    break;
                case "20":
                    addTips = (20 / 100) * total;
                    break;
                default:
                    break;
            }
            this.setState({ activeButton: e.value, tips: addTips });
        }
    }

    render() {
        const { items, tips, total } = this.state;
        console.log(this.state.tips)
        return (
            <div className="fullCard">
                <Header></Header>
                <div className="cardSize">
                    <Col>
                        <Card className="col-md text-center mt-4 cards">
                            <Card.Body>
                                <Card.Text>
                                    <div className="cardTitle">Tang and Biscuit</div>
                                    <div className="cardTitleText">Table #3</div>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card className="col-md mt-5 cards">
                            <Card.Body>
                                <Card.Text>
                                    {
                                        items && items._embedded && items._embedded.items && items._embedded.items.map(data => {
                                            return (
                                                <div className="lineHeight">
                                                    <span className="items">{data.name}</span>
                                                    <span className="itemsPrice">${data.price}</span>
                                                </div>
                                            )
                                        })
                                    }
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card className="col-md mt-5 cards">
                            <Card.Body>
                                <Card.Text>
                                    <div className="lineHeight">
                                        <span className="items">Subtotal</span>
                                        <span className="itemsPrice">${items && items.totals && items.totals.sub_total}</span>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <ButtonGroup aria-label="Basic example" className="button-group btn-group btn-group-sm col-md-5 mt-5">
                            <Button value="default" onClick={e => this.isActive(e.target)} className={this.state.activeButton === "default" ? "btn-active-button" : ""}>15%</Button>
                            <Button value="18" onClick={e => this.isActive(e.target)} className={this.state.activeButton === "18" ? "btn-active-button" : ""}>18%</Button>
                            <Button value="20" onClick={e => this.isActive(e.target)} className={this.state.activeButton === "20" ? "btn-active-button" : ""}>20%</Button>
                        </ButtonGroup>
                    </Col>
                    <Col>
                        <Card className="col-md mt-5 cards">
                            <Card.Body>
                                <Card.Text>
                                    <div className="lineHeight">
                                        <span className="items">Total</span>
                                        <span className="itemsPrice">${total + tips}</span>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Button className="google mt-5">
                            <img src={image} alt="google"></img>
                        </Button>
                    </Col>
                </div>
            </div>
        )
    }
}

export default WithAction(Bill);
