import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BillComponemt from './bill';
import { getItems } from '../../redux/actions/bill.action';


const mapStateToProps = state => {
    return {
        bill: state.Bill.billResponse,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ getItems }, dispatch);
};

const billRequest = connect(mapStateToProps, mapDispatchToProps);

export default billRequest;